# Example

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

# Custom commands

## Remove docker containers

Run `npm run docker-remove-containers` to remove all containers with name like `container-angular`.

## Remove docker images

Run `npm run docker-remove-images` to remove all images with name like `angular`.

## Remove docker containers and images

Run `npm run docker-clean` to run `docker-remove-containers` and `docker-remove-images`.

## Remove docker container homologacao

Run `npm run docker-remove-container-homologacao` to remove all containers with name like `container-angular-homologacao`.

## Remove docker image homologacao

Run `npm run docker-remove-image-homologacao` to remove all images with name like `angular-homologacao`.

## Remove docker container and image homologacao

Run `npm run docker-clean-homologacao` to run `docker-remove-container-homologacao` and `docker-remove-image-homologacao`.

## Build image homologacao

Run `npm run docker-image-homologacao` to run the `Dockerfile` and build an image with the name `angular-homologacao`.

## Build project in homologacao mode and create image

Run `npm run build-homologacao` to build the project in homologacao mode and run `docker-image-homologacao`.

## Run image homologacao

Run `npm run build-homologacao` to start a container using the image `angular-homologacao`.

## Erase homologacao and build it again

Run `npm run homologacao` to run `docker-clean-homologacao`, `build-homologacao` and `container-homologacao`.

## Remove docker container producao

Run `npm run docker-remove-container-producao` to remove all containers with name like `container-angular-producao`.

## Remove docker image producao

Run `npm run docker-remove-image-producao` to remove all images with name like `angular-producao`.

## Remove docker container and image producao

Run `npm run docker-clean-producao` to run `docker-remove-container-producao` and `docker-remove-image-producao`.

## Build image producao

Run `npm run docker-image-producao` to run the `Dockerfile` and build an image with the name `angular-producao`.

## Build project in producao mode and create image

Run `npm run build-producao` to build the project in producao mode and run `docker-image-producao`.

## Run image producao

Run `npm run build-producao` to start a container using the image `angular-producao`.

## Erase producao and build it again

Run `npm run producao` to run `docker-clean-producao`, `build-producao` and `container-producao`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
